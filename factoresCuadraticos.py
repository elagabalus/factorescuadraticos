'''
Santander Martínez Ángel Antonio
Fecha 14 de octubre del 2019
Método de los factores cuadraticos
'''
from math import * #se importan todas las funciones y constantes de la blbioteca math
import argparse #se importan el modulo para implementar un menù de linea de comandos
from ecuaciónCuadratica import formulaGeneral #Se importa el modulo para resolver una ecuacion con
# la formula general de 2º grado
import logging as log #se importa el modulo para depuracion para tener diferentes opciones de detalle
import sys
# Esta funcion imprime en pantalla los polinomios en un formato legible por el usuario
def printArrayPolynom(array:float):->None
    n=len(array) # Se determina el grado del polinomio a traves del numero de coeficientes
    aux="" # cadena auxiliar a la que agregará el resultado
    for i in range(n): # para cada coeficiente desde 0 hasta el grado del polinomio
        coef=array[i] # se extrae el coeficiente de la lista que contiene a los coeficientes del polinomio
        if coef!=0 and (n-i-1)>1:
            if abs(coef)==1:
                if coef<0:
                    aux+=f"-(x**{n-i-1})"
                else:
                    aux+=f"(x**{n-i-1})"
            else:
                if coef<0:
                    aux+=f"-({abs(coef)}x**{n-i-1})"
                else:
                    aux+=f"+({abs(coef)}x**{n-i-1})"
        else:
            if coef!=0:
                if (n-i-1)==1:
                    if coef<0:
                        aux+=f"-({abs(coef)}x)"
                    else:
                        aux+=f"+({abs(coef)}x)"
                else:
                    if coef<0:
                        aux+=f"-({abs(coef)}"
                    else:
                        aux+=f"+({abs(coef)})"
    return aux

'''
Se crea con ayuda de la biblioteca argsparse para capturar el polinio a resolver
'''
def menuCli():
    parser=argparse.ArgumentParser(description='Método de factores cuadraticos')
    parser.add_argument('coeficiente_p','p',type=float,
                    help='Estimación inicial del coeficiente p')
    parser.add_argument('coeficiente_q','q',type=float,
                    help='Estimación inicial del coeficiente q')
    parser.add_argument('precision',type=float,
                    help='Precisión requerida')
    parser.add_argument('-c','--coeficientes',nargs='+',type=float,
                    help='Coeficientes x_n,x_n-1,.. del polinio de que e desea obtener las raices')
    parser.add_argument('--v','--procedimiento_detallado',action='store_true',
					help='Imprimir en pantalla todos los pasos realizados')
    args = parser.parse_args()
    return args
'''
Implementacion del metodo de los factores cuadraticos
donde hay un polinomio P(x)=(a_0)x**(n)+(a_1)x**(n-1)+...+(a_n-1)x**(1)+(a_n)x**(0) de grado n
del que se deben obtener las raices
se divide por un polinomio con coeficientes arbitrarios p y q Q(x)=x**2+px+q
y se optiene un polinomio de residuo
R(x)=(b0)(x**n)+(pb0)(x**n-1)(qb0)(x**n-2)+(b1)(x**n-1)+(pb0)(x**n-2)(qb1)(x**n-3)+...+(pb_n-3)x+(qb_n-2)+Rx+S
'''
def factoresCuadraticos(coeficientes,coeficiente_pInicial,ceficiente_qInicial,tolerancia):
    if coeficientes:
        coeficientes_a=coeficientes
        orden=len(coeficientes)-1
    else:
        coeficientes_a=[]
        orden=int(input("Introduce el oreden del polinomio que desea factorizar: "))
        i=orden
        while(i>=0):
            if i!=0:
                coeficientes_a.append(int(input(f"Introduce el coeficiente de la variable x^{i}: ")))
            else:
                coeficientes_a.append(int(input(f"Introduce el termino independiente: ")))
            i-=1
    coeficiente_p=coeficiente_pInicial
    coeficiente_q=coeficiente_qInicial
    coeficientes_b=[0 for x in range(0,orden-1)]

    for i in range(0,orden-1):
        coeficientes_b[i]=a[i]
        if i-1>=0:
            coeficientes_b[i]=coeficientes_b[i]-(coeficiente_p*coeficientes_b[i-1])
            if i-2>=0:
                coeficientes_b[i]=coeficientes_b[i]-(coeficiente_q*coeficientes_b[i-2])
    coeficiente_R=coeficientes_a[orden-1]-coeficiente_p*coeficientes_b[orden-2]-coeficiente_q*coeficientes_b[orden-3]
    coeficiente_S=coeficientes_a[orden]-coeficiente_q*coeficientes_b[orden-2]
    tolerancia=tolerancia
    k=0
    while(abs(coeficiente_R)>tolerancia or abs(coeficiente_S)>tolerancia):
        k+=1
        log.info(f"Iter {k} p={coeficiente_p} q={coeficiente_q} R={coeficiente_R} S={coeficiente_S} ")
        log.info(f"coeficientes {coeficiente_b}")
        for i in range(0,orden-1):
            coeficientes_b[i]=coeficientes_a[i]
            if i-1>=0:
                coeficientes_b[i]-=coeficiente_p*coeficientes_b[i-1]
                if i-2>=0:
                    coeficientes_b[i]-=coeficiente_q*coeficientes_b[i-2]
        coeficiente_R=coeficientes_a[orden-1]-(coeficiente_p*coeficientes_b[orden-2])-(coeficiente_q*coeficientes_b[orden-3])
        coeficiente_S=coeficientes_a[orden]-(coeficiente_q*coeficientes_b[orden-2])
        diferencia_coeficiente_p_anteorior=coeficiente_R/coeficientes_b[orden-2]
        diferencia_coeficiente_q_anteorior=coeficiente_S/coeficientes_b[orden-2]
        log.info(f"Δp={diferencia_coeficiente_p_anteorior} Δq={diferencia_coeficiente_q_anteorior}")
        coeficiente_q+=diferencia_coeficiente_q_anteorior
        coeficiente_p+=diferencia_coeficiente_p_anteorior
    print(f"Función dada {printArrayPolynom(coeficientes_a)}")
    factorCuadratico=[1,coeficiente_p,coeficiente_q]
    raiz_1,raiz_2=formulaGeneral(*factorCuadratico)
    print(f"Factor cuadratico {printArrayPolynom(factorCuadratico)}")
    print(f"Polinomio residuo {printArrayPolynom(coeficientes_b)}")
    print(f"Raíces encontradas x1={raiz_1}, x2={raiz_2}")
'''
Mètodo que comprueba todas las opciones seleccionadas del menú de linea de comandos

'''
def main():
    args=menuCli()
    if args.procedimiento_detallado:
        log.basicConfig(stream=sys.stdout,format="%(message)s", level=log.INFO)
    else:
        log.basicConfig(format="%(levelname)s: %(message)s")
    if args.coeficientes:
        factoresCuadraticos(args.coeficientes,args.coeficiente_p,args.coeficiente_q,args.precision)
    else:
        factoresCuadraticos(False,args.coeficiente_p,args.coeficiente_q,args.precision)
if __name__=="__main__":
    main()
